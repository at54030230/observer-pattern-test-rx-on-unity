﻿
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Subject<T> : IObserver<T>, IObservable<T>
{
    private readonly LinkedList<IObserver<T>> observers = new LinkedList<IObserver<T>>();
    private bool isCompleted;
    private Exception exception = null;

    public IDisposable Subscribe(IObserver<T> observer)
    {
        if (isCompleted)
        {
            observer.OnCompleted();
        }
        else if (exception != null)
        {
            observer.OnError(exception);
        }
        else
        {
            if (!this.observers.Contains(observer))
                this.observers.AddLast(observer);
        }
        return new UnSubscriber(observers, observer);
    }

    public class UnSubscriber : IDisposable
    {
        private readonly LinkedList<IObserver<T>> observers = null;
        private readonly IObserver<T> observer = null;

        public UnSubscriber(LinkedList<IObserver<T>> observers, IObserver<T> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (this.observers.Contains(this.observer))
                this.observers.Remove(this.observer);
        }
    }

    public void OnNext(T value)
    {
        foreach (var observer in this.observers)
        {
            if (value == null) observer.OnError(new ArgumentNullException());
            else observer.OnNext(value);
        }
    }

    public void OnCompleted()
    {
        isCompleted = true;
        foreach (var observer in this.observers)
            observer.OnCompleted();
        this.observers.Clear();
    }

    public void OnError(Exception error)
    {
        exception = error;
        foreach (var observer in this.observers)
            observer.OnError(error);
        this.observers.Clear();
    }


}
