﻿using UnityEngine;
using System.Collections;
using System;

public class AnonymousObservable<T> : IObservable<T> {

    private readonly Func<IObserver<T>, IDisposable> subsucribe;

    public AnonymousObservable(Func<IObserver<T>,IDisposable> subsucribe)
    {
        if (subsucribe == null)
            throw new ArgumentNullException("subscribe == null");
        this.subsucribe = subsucribe;
    }

    public IDisposable Subscribe(IObserver<T> observer)
    {
        return this.subsucribe(observer);
    }

}
