﻿using UnityEngine;
using System.Collections;
using System;

public static class ObservableFactory  {

    public static IObservable<T> Never<T>()
    {
        return new Subject<T>();
    }

    public static IObservable<T> Empty<T>()
    {
        var subject = new Subject<T>();
        subject.OnCompleted();
        return subject;
    }

    public static IObservable<T> Throw<T>(Exception error)
    {
        var subject = new Subject<T>();
        subject.OnError(error);
        return subject;
    }

    public static IObservable<int> Range(int start,int end,int step=1)
    {
        var subject = new ReplaySubject<int>();
        for (int i = start;i< end; i += step)
        {
            subject.OnNext(i);
        }
        subject.OnCompleted();
        return subject;
    }

    public static IObservable<T> Repeat<T>(T element, int count)
    {
        var subject = new ReplaySubject<T>();
        for (int i=0;i< count;i++)
            subject.OnNext(element);
        subject.OnCompleted();
        return subject;
    }


}
