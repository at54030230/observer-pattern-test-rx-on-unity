﻿using UnityEngine;
using System.Collections;
using System;

public class AnonymousObserver<T> : IObserver<T>
{
    private readonly Action onCompleted = null;
    private readonly Action<Exception> onError = null;
    private readonly Action<T> onNext = null;

    public AnonymousObserver(Action<T> onNext, Action<Exception> onError, Action onCompleted)
    {
        this.onNext = onNext;
        this.onError = onError;
        this.onCompleted = onCompleted;
    }

    public void OnCompleted()
    {
        if (this.onCompleted != null) this.onCompleted();
    }

    public void OnError(Exception error)
    {
        if (this.onError != null) this.onError(error);
    }

    public void OnNext(T value)
    {
        if (this.onNext != null) this.onNext(value);
    }
}