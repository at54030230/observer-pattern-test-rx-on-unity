﻿using UnityEngine;
using System.Collections;
using System;

public static class ObservableHelper {

    static void Log(string str,params object[] objects)
    {
        Debug.Log(String.Format(str, objects));
    }

    // FOR DEBUG
    public static IDisposable SubscribeTracer<T>(this IObservable<T> source, string name)
    {
        Log("----- {0} : Subscribe Before -----", name);
        var disposer = source.Subscribe
        (
            value => Log("{0} : OnNext({1})", name, value),
            error => Log("{0} : OnError({1})",name, error),
            () => Log("{0} : OnCompleted", name)
        );
        Log("----- {0} : Subscribe After -----", name);
        return disposer;
    }
    //For DEUB
    public static void DisposeTracer(this IDisposable source, string name)
    {
        Log("----- {0} : Dispose Before -----", name);
        source.Dispose();
        Log("----- {0} : Dispose After -----", name);
    }

    public static IDisposable Subscribe<T>(this IObservable<T> source,Action<T> onNext,Action<Exception> onError,Action onCompleted)
    {
        return source.Subscribe(new AnonymousObserver<T>(onNext, onError, onCompleted));
    }
    public static IDisposable Subscribe<T>(this IObservable<T> source, Action<T> onNext, Action onCompleted)
    {
        return source.Subscribe(new AnonymousObserver<T>(onNext, null, onCompleted));
    }
    public static IDisposable Subscribe<T>(this IObservable<T> source, Action<T> onNext)
    {
        return source.Subscribe(new AnonymousObserver<T>(onNext, null,null));
    }

    /// <summary>
    /// 指定のIObservableから，何も通知せず，Dispose()だけができる（意味のない）IObservableを作成します．
    /// </summary>
    public static IObservable<T> Disposer<T>(this IObservable<T> source)
    {
        return new AnonymousObservable<T>(
            observer =>
            {
                IDisposable disposer = null;
                disposer = source.Subscribe(
                    value => { },
                    expection => { },
                    () => { });
                return disposer;
            }
            );
    }

    public static IObservable<T> Where<T>(this IObservable<T> source,Func<T,bool> predicate)
    {
        return new AnonymousObservable<T>(
            observer =>
            {
                IDisposable disposer = null;
                disposer = source.Subscribe(
                    value => {
                        try
                        {
                            if (!predicate(value))
                                return;
                        }
                        catch (Exception error)
                        {
                            observer.OnError(error);
                            disposer.Dispose();
                            return;
                        }
                        observer.OnNext(value);
                    },
                    observer.OnError,
                    observer.OnCompleted);
                return disposer;
            }
            );
    }

    public static IObservable<TResult> Select<T, TResult>(this IObservable<T> source, Func<T, TResult> selector)
    {
        return new AnonymousObservable<TResult>(observer =>
        {
            IDisposable disposer = null;
            disposer = source.Subscribe(value =>
            {
                var result = default(TResult);
                try
                {
                    result = selector(value);
                }
                catch (Exception error)
                {
                    observer.OnError(error);
                    disposer.Dispose();
                    return;
                }
                observer.OnNext(result);
            },
            observer.OnError,
            observer.OnCompleted);
            return disposer;
        });
    }

}
