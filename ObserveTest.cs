﻿using UnityEngine;
using System.Collections;
using System;

public class ObserveTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Test5();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void Test5()
    {
        var vable = ObservableFactory.Range(0, 10).Where(v => v % 2 == 0).Select(v => v * 2);
        vable.SubscribeTracer("A");
    }

    void Test4()
    {
        var vable = ObservableFactory.Range(2, 5);
        vable.SubscribeTracer("A");
    }
    void Test3()
    {
        var vable = ObservableFactory.Throw<int>(new ArgumentNullException());
        vable.SubscribeTracer("A");
    }

    void Test2()
    {
        var subject = new Subject<int>();
        var disposerA = subject.SubscribeTracer("A");
        subject.OnNext(1);
        var disposerB = subject.SubscribeTracer("B");
        subject.OnNext(2);
        disposerA.DisposeTracer("A");
        subject.OnNext(3);
        subject.OnCompleted();
        var disposerC = subject.SubscribeTracer("C");
        subject.OnNext(4);
    }

    void Test1()
    {
        var provider = new Provider<int>();
        var us1 = provider.Subscribe(
            value => Debug.Log(value),
            error => Debug.Log(error),
            () => Debug.Log("Completed")
            );
        provider.Deliver(1);
        provider.Deliver(10);
        provider.StopDelivering();
        provider.Deliver(100);
    }
    
}
