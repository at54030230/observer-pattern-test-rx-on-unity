﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Provider<T> : IObservable<T> {

    private readonly LinkedList<IObserver<T>> observers = new LinkedList<IObserver<T>>();

    public IDisposable Subscribe(IObserver<T> observer)
    {
        if (!this.observers.Contains(observer))
            this.observers.AddLast(observer);
        return new UnSubscriber(observers, observer);
    }

    public class UnSubscriber : IDisposable
    {
        private readonly LinkedList<IObserver<T>> observers = null;
        private readonly IObserver<T> observer = null;

        public UnSubscriber(LinkedList<IObserver<T>> observers, IObserver<T> observer)
        {
            this.observers = observers;
            this.observer = observer;
        }

        public void Dispose()
        {
            if (this.observers.Contains(this.observer))
                this.observers.Remove(this.observer);
        }
    }

    public void Deliver(T value)
    {
        foreach (var observer in this.observers)
        {
            if (value == null) observer.OnError(new ArgumentNullException());
            else observer.OnNext(value);
        }
    }

    public void StopDelivering()
    {
        foreach (var observer in this.observers)
            observer.OnCompleted();
        this.observers.Clear();
    }

}
