﻿using UnityEngine;
using System.Collections;
using System;

public interface IObservable<out T>
{
    IDisposable Subscribe(IObserver<T> observer);
}
